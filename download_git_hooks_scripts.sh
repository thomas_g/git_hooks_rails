#!/bin/bash

mkdir -p scripts

repo="https://gitlab.com/thomas_g/git_hooks_rails/raw/master"

files=(pre-commit.sh pre-push.sh setup.sh git_hooks.sh)
for file in "${files[@]}"
do
  printf '%.s/' {1..80}
  echo
  if [ -f "scripts/${file}" ]; then
    echo "${file} already exists, skipping."
  else
    echo "Downloading ${file}"
    curl -o "scripts/${file}" "${repo}/${file}"
  fi
  echo
done

echo "make all files in scripts/ executable"
chmod +x scripts/*

config_files=(rubocop haml-lint)
for config_file in "${config_files[@]}"
do
  file_name=".$config_file.yml"
  if [ ! -f "${file_name}" ]; then
    read -p "Do you want to use the $config_file config from this repo ? (y/N - enter to skip)?" prompt
    if [[ $prompt == "y" ]]; then
      echo "Downloading $file_name"
      curl -o "${file_name}" "${repo}/${file}"
    else
      echo "Skipping $file_name"
    fi
    echo
  fi
done

echo "All files downloaded ! You can now execute the setup.sh script to create symlinks to the hooks."

exit 0
