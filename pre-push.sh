#!/bin/bash

. "$PWD/scripts/git_hooks.sh"

action="BRAKEMAN"
title="Check for vulnerabilities using Brakeman."
command="bundle exec brakeman &> /dev/null"
perform_hook

action="RSPEC"
title="Run test suite."
command="bundle exec rspec --fail-fast &> /dev/null"
perform_hook

action="BUNDLER AUDIT"
title="Check for vulnerable gems."
command="(bundle audit update; bundle audit check) &> /dev/null"
perform_hook
