#!/bin/bash

. "$PWD/scripts/git_hooks.sh"

action="PREVENT COMMIT MASTER"
title="Prevent commit on master branch."
command="[ `git rev-parse --abbrev-ref HEAD` != 'master' ]"
perform_hook

action="BUNDLE CHECK"
title="Check if Gemfile's dependencies are satisfied."
command="bundle check &> /dev/null"
perform_hook

action="RUBOCOP"
title="Enforce Rails best practices using Rubocop."
command="bundle exec rubocop --fail-fast &> /dev/null"
perform_hook

action="HAML LINT"
title="Enforce HAML best practices using Haml-lint."
command="bundle exec haml-lint --fail-fast &> /dev/null"
perform_hook
