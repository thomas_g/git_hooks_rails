#!/bin/bash

# This script will install gems from your gemfile, setup your database
# and create symlinks to git hooks
# To remove hooks:
# rm .git/hooks/pre-commit
# rm .git/hooks/pre-push

grep -qF "$LINE" ~/.gemrc || echo "$LINE" >> ~/.gemrc

printf '%.s/' {1..80}
echo
echo "Installing gems"
gem install bundler --conservative
bundle check || bundle install
echo

number_of_cores=$(nproc --all)
if [ $number_of_cores -gt 1 ]; then
  bundle config --local jobs $(($number_of_cores - 1))
fi

printf '%.s/' {1..80}
echo
echo "Setting up database"
rails db:migrate 2>/dev/null || rails db:setup
echo

printf '%.s/' {1..80}
echo
echo "Installing Git hooks"

GIT_DIR=$(git rev-parse --git-dir)

if [ -f "$GIT_DIR/hooks/pre-commit" ] || [ -f "$GIT_DIR/hooks/pre-push" ]; then
  echo "There is already a pre-commit or pre-push hooks installed. Delete them first."
  exit 2
else
  ln -s ../../scripts/pre-commit.sh $GIT_DIR/hooks/pre-commit
  ln -s ../../scripts/pre-push.sh $GIT_DIR/hooks/pre-push
  echo 'Symlink to git hooks installed !'
fi
