#!/bin/bash

# Some GUI (ie Gitx) won't source your .bashrc
source ~/.rvm/scripts/rvm

HOOK_NAME="$(basename -- ${0})"

printf '%.s/' {1..80}
echo
echo "Executing Git Hook for $HOOK_NAME"
echo "You can bypass git hooks using --no-verify"
echo

perform_hook () {
  printf '%.s*' {1..50}
  echo
  echo $title
  printf '%.s*' {1..20}
  echo

  $(eval "$command")
  if [ $? -ne 0 ]; then
    echo "⛔️ [$action] Failed ⛔️"
    exit 1
  else
    echo "[$action] --> 🤙 OK !"
    echo
  fi
}
