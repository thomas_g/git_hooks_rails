# Git Hooks for Rails

This project downloads Git hooks for Ruby on Rails projects.

## Hooks

### Pre-commit
* Prevents that you commit code on master.
* Check if the gemfile's dependencies are satisfied.
* Runs Rubocop and fails after the first offense.
* Runs Haml-Lint and fails after the first offense.

### Pre-push
* Runs Brakeman.
* Runs our Rspec suite and fails after the first broken test.
* Runs Bundler Audit.

## Requirements

You should at least have Brakeman, Bundler Audit, Rspec and Rubocop installed.  
<https://github.com/rspec/rspec-rails>
<https://github.com/presidentbeef/brakeman>
<https://github.com/rubocop-hq/rubocop-rails>
<https://github.com/rubysec/bundler-audit>

In your gemfile:
```ruby
group :development do
  gem 'brakeman', require: false
  gem 'bundler-audit', require: false
  gem 'haml_lint', require: false
  gem 'rspec', require: false
  gem 'rubocop-rails', require: false
end
```
You can always modify the hooks scripts if you don't need one of them.

## Installation

Download the script files:
```bash
curl https://gitlab.com/thomas_g/git_hooks_rails/raw/master/download_git_hooks_scripts.sh | bash
```

Then run:
```bash
setup.sh
```

## Usage

The script setup.sh will install gems from your gemfile, setup your database, download config files for Rubocop and Haml-Lint and create symlinks to Git hooks.

If you want to use the Rubocop config file from this repository, you will need to add two extra gems to your Gemfile:
```ruby
group :development do
  gem 'rubocop-gitlab-security'
  gem 'rubocop-rspec'
end
```

## Improvement

- Run Rubocop and Haml-Lint only on modified files.
